import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class CaserCiperProject {
    static Path filepath = Paths.get(System.getProperty("user.dir") + "\\src\\message.txt");
    public static void main(String[] args) {
        Scanner getInput = new Scanner(System.in);
        Scanner getInput1 = new Scanner(System.in);
        int index;
        String file = "";
        String translate = "";
        String translate1 = "";
        ArrayList<Character> key = new ArrayList<>();


        for (char ch = 'a'; ch <= 'z'; ch++) {
            key.add(ch);
        }
        for (char ch = 'A'; ch <= 'Z'; ch++) {
            key.add(ch);
        }

        //encrypt the message from user input
        System.out.println();
        System.out.println("Do you wish to encrypt or decrypt a message?\n");
        String eord = getInput.next();
        System.out.println();
        System.out.println("Enter your message:\n");
        String message = getInput1.nextLine();
        System.out.println();
        System.out.println("Enter the key number (1-52)\n");
        int num = getInput.nextInt();
        System.out.println();

        for (int i = 0; i < message.length(); i++) {
            if (message.charAt(i) == ' ' || message.charAt(i) == '.' || message.charAt(i) == ',' || message.charAt(i) == '?') {
                translate += message.charAt(i);
            } else if(key.indexOf(message.charAt(i)) + num >= 52 && eord.equals("encrypt")) {
                index = (key.indexOf(message.charAt(i)) + num) - 52;
                translate += key.get(index);
            } else if (key.indexOf(message.charAt(i)) + num < 52 && eord.equals("encrypt")) {
                translate += key.get(key.indexOf(message.charAt(i)) + num);
            }

        }

        System.out.println("Your translated text is:");
        System.out.println();
        System.out.println((translate));
        System.out.println();

        //decrypt the message from a file
        try {
            //Files.createFile(filepath);
            Files.write(filepath, translate.getBytes());
            file = Files.readString(filepath);
        } catch (Exception e) {
            e.getStackTrace();
        }

        System.out.println("Do you wish to encrypt or decrypt a message?\n");
        eord = getInput.next();
        System.out.println();
        System.out.println("Enter your message:\n");
        System.out.println(file);
        System.out.println();
        System.out.println("Enter the key number (1-52)\n");
        num = getInput.nextInt();
        System.out.println();

        for (int i = 0; i < file.length(); i++) {
            if (file.charAt(i) == ' ' || file.charAt(i) == '.' || file.charAt(i) == ',' || file.charAt(i) == '?') {
                translate1 += file.charAt(i);
            } else if(key.indexOf(file.charAt(i)) - num < 0 && eord.equals("decrypt")) {
                index = (key.indexOf(file.charAt(i)) - num) + 52;
                translate1 += key.get(index);
            } else if (key.indexOf(file.charAt(i)) - num >= 0 && eord.equals("decrypt")) {
                translate1 += key.get(key.indexOf(file.charAt(i)) - num);
            }

        }

        System.out.println("Your translated text is:");
        System.out.println();
        System.out.println((translate1));
        System.out.println();

    }
}
